#include <linux/atomic.h>
#include <linux/cdev.h>
#include <linux/completion.h>
#include <linux/if_packet.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/net.h>
#include <linux/socket.h>
#include <linux/spinlock.h>
#include <linux/timer.h>
#include <linux/uaccess.h>
#include <linux/workqueue.h>
#include <net/net_namespace.h>
#include <net/sock.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Alejandro Díaz Pereira");
MODULE_DESCRIPTION("Arm driver for OS project");
MODULE_VERSION("1.0.0");

#define ETH_P_P3OP    0x6969
#define MAX_DEVS      10
#define HEARTBEAT_MS  250
#define TIMEOUT_BEATS 4

struct p3op_msg {
	u8 magic[4];
	u8 base;
	u8 shoulder;
	u8 elbow;
} __packed;

struct p3op_pos {
	u8 base;
	u8 shoulder;
	u8 elbow;
};

static struct class *device_class;
static dev_t		 device_major;

static struct socket *p3op_sock;
static struct workqueue_struct *p3op_wq;
static struct work_struct p3op_rx_work;
static struct work_struct p3op_tx_work;

static struct p3op_node {
	atomic_long_t      dev;
	atomic_t           dead_heartbeats;
	atomic_t           needs_sync;
	struct sockaddr_ll addr;
	struct timer_list  timer;
	struct p3op_pos    actual;
	struct p3op_pos    target;
	struct mutex       open_mutex;
	struct completion  settled;
} p3op_nodes[MAX_DEVS] = {};

static DEFINE_SPINLOCK(p3op_nodes_lock);

static inline bool p3op_node_settled(const struct p3op_node *node)
{
	return node->actual.base == node->target.base
	    && node->actual.shoulder == node->target.shoulder
	    && node->actual.elbow == node->target.elbow;
}

static int p3op_open(struct inode *inode, struct file *file)
{
	int rc;

	unsigned minor;
	struct p3op_node *node;

	minor = iminor(inode);
	if (minor >= MAX_DEVS)
		return -ENODEV;

	node = &p3op_nodes[minor];

	rc = mutex_lock_interruptible(&node->open_mutex);
	if (rc >= 0)
		file->private_data = node;

	return rc;
}

static int p3op_release(struct inode *inode, struct file *file)
{
	struct p3op_node *node = file->private_data;

	mutex_unlock(&node->open_mutex);
	return 0;
}

static ssize_t p3op_read(struct file *file, char __user *buf, size_t count, loff_t *ppos) 
{
	ssize_t ret;
	struct p3op_pos pos;
	struct p3op_node *node = file->private_data;

	if (*ppos >= sizeof pos)
		return 0;
	else if (count < sizeof pos)
		return -EINVAL;

	ret = sizeof pos;

	if ((void *)atomic_long_read(&node->dev)) {
		spin_lock(&p3op_nodes_lock);
		memcpy(&pos, &node->actual, sizeof pos);
		spin_unlock(&p3op_nodes_lock);
	} else
		ret = -ENOLINK;

	if (ret >= 0 && copy_to_user(buf, &pos, sizeof pos))
		ret = -EFAULT;

	if (ret >= 0)
		*ppos += ret;

	return ret;
}

static ssize_t p3op_write(struct file *file, const char __user *buf, size_t count, loff_t *ppos) 
{
	int rc;
	bool queue_tx;
	ssize_t ret;
	struct p3op_pos pos;
	struct p3op_node *node = file->private_data;

	if (count != sizeof pos)
		return -EINVAL;
	else if (copy_from_user(&pos, buf, sizeof pos))
		return -EFAULT;

	ret = sizeof pos;

	while (1) {
		queue_tx = false;

		if ((void *)atomic_long_read(&node->dev)) {
			spin_lock(&p3op_nodes_lock);

			memcpy(&node->target, &pos, sizeof pos);
			queue_tx = !p3op_node_settled(node);

			spin_unlock(&p3op_nodes_lock);
		} else
			ret = -ENOLINK;

		if (!queue_tx)
			break;

		atomic_set(&node->needs_sync, 1);
		queue_work(p3op_wq, &p3op_tx_work);

		rc = wait_for_completion_interruptible(&node->settled);
		if (rc < 0) {
			ret = rc;
			break;
		}
	}

	if (ret >= 0 && !(void *)atomic_long_read(&node->dev))
		ret = -ENOLINK;

	if (ret >= 0)
		*ppos += ret;

	return ret;
}

static const struct file_operations p3op_fops = {
	.owner   = THIS_MODULE,
	.open    = p3op_open,
	.read    = p3op_read,
	.write   = p3op_write,
	.release = p3op_release,
};

static void p3op_heartbeat(struct timer_list *timer)
{
	struct p3op_node *node = container_of(timer, struct p3op_node, timer);

	spin_lock(&p3op_nodes_lock);

	if (likely((void *)atomic_long_read(&node->dev))) {
		if (likely(atomic_add_return(1, &node->dead_heartbeats) < TIMEOUT_BEATS))
			mod_timer(timer, jiffies + msecs_to_jiffies(HEARTBEAT_MS));

		atomic_set(&node->needs_sync, 1);
		queue_work(p3op_wq, &p3op_tx_work);
	}

	spin_unlock(&p3op_nodes_lock);
}

static int p3op_new(const struct sockaddr_ll *peer, const struct p3op_pos *start_pos)
{
	int rc, minor;
	char name[16];
	dev_t device_no;
	struct device *dev;
	struct p3op_node *node = NULL;
	struct net_device *net_dev;

	net_dev = dev_get_by_index(&init_net, peer->sll_ifindex);

	spin_lock(&p3op_nodes_lock);

	for (minor = 0; minor < MAX_DEVS; ++minor)
		if (!(void *)atomic_long_read(&p3op_nodes[minor].dev)) {
			node = &p3op_nodes[minor];
			break;
		}

	if (!node) {
		spin_unlock(&p3op_nodes_lock);
		pr_warn("out of allocatable nodes\n");

		dev_put(net_dev);
		return -ENOBUFS;
	}

	snprintf(name, sizeof name - 1, "p3op%d", minor);
	name[sizeof name - 1] = '\0';

	device_no = MKDEV(device_major, minor);
	dev = device_create(device_class, NULL, device_no, NULL, name);

	if (IS_ERR(dev)) {
		spin_unlock(&p3op_nodes_lock);
		rc = PTR_ERR(dev);

		pr_err("can't create device with error %d\n", rc);
		return rc;
	}

	atomic_long_set(&node->dev, (long)dev);
	memcpy(&node->addr, peer, sizeof *peer);
	memcpy(&node->actual, start_pos, sizeof *start_pos);
	memcpy(&node->target, start_pos, sizeof *start_pos);

	atomic_set(&node->needs_sync, 1);
	atomic_set(&node->dead_heartbeats, 0);

	mod_timer(&node->timer, jiffies + msecs_to_jiffies(HEARTBEAT_MS));

	spin_unlock(&p3op_nodes_lock);
	pr_info("%s: bound to %pM on interface %s\n", name, peer->sll_addr, net_dev->name);

	dev_put(net_dev);

	queue_work(p3op_wq, &p3op_tx_work);
	return minor;
}

static void p3op_rx(struct work_struct *work)
{
	int minor;
	bool was_settled;
	ssize_t msglen;
	struct kvec iov;
	struct p3op_pos pos;
	struct p3op_msg recv;
	struct p3op_node *node;
	struct msghdr msg = { };
	struct sockaddr_ll sockaddr;

	(void)work;

	msg.msg_name = (struct sockaddr *)&sockaddr;
	msg.msg_namelen = sizeof sockaddr;

	while (1) {
		iov.iov_base = &recv;
		iov.iov_len = sizeof recv;

		msglen = kernel_recvmsg(p3op_sock, &msg, &iov, 1, sizeof recv, MSG_DONTWAIT);
		if (msglen < 0) {
			if (msglen != -EAGAIN)
				pr_err("recv error: %zd\n", msglen);

			break;
		} else if (msglen != sizeof recv) {
			pr_warn("bad length in recv: expected %zu, got %zd\n", sizeof recv, msglen);
			continue;
		} else if (recv.magic[0] != 'p' || recv.magic[1] != '3'
		        || recv.magic[2] != 'o' || recv.magic[3] != 'p') {
			pr_warn("bad magic in recv\n");
			continue;
		}

		pos.base = recv.base;
		pos.shoulder = recv.shoulder;
		pos.elbow = recv.elbow;

		spin_lock(&p3op_nodes_lock);
		for (minor = 0; minor < MAX_DEVS; ++minor) {
			node = &p3op_nodes[minor];
			if ((void *)atomic_long_read(&node->dev)
			 && node->addr.sll_ifindex == sockaddr.sll_ifindex
			 && !memcmp(&sockaddr.sll_addr, &node->addr.sll_addr, sockaddr.sll_halen)) {
				was_settled = p3op_node_settled(node);

				node->actual.base = recv.base;
				node->actual.shoulder = recv.shoulder;
				node->actual.elbow = recv.elbow;
				atomic_set(&node->dead_heartbeats, 0);

				if (!was_settled && p3op_node_settled(node))
					complete(&node->settled);

				break;
			}
		}
		spin_unlock(&p3op_nodes_lock);

		if (minor == MAX_DEVS) {
			minor = p3op_new(&sockaddr, &pos);
			if (minor < 0) {
				pr_err("p3op_new() failed with error %d\n", minor);
				continue;
			}
		}
	}
}

static void p3op_tx(struct work_struct *work)
{
	int minor, destroyed;
	ssize_t msglen;
	struct kvec iov;
	struct p3op_pos pos;
	struct p3op_msg send;
	struct p3op_node *node;
	struct msghdr msg = { };
	struct sockaddr_ll peer;

	(void)work;

	msg.msg_name = (struct sockaddr *)&peer;
	msg.msg_namelen = sizeof peer;

	send.magic[0] = 'p';
	send.magic[1] = '3';
	send.magic[2] = 'o';
	send.magic[3] = 'p';

	while (1) {
		spin_lock(&p3op_nodes_lock);

		destroyed = 0;
		for (minor = 0; minor < MAX_DEVS; ++minor) {
			node = &p3op_nodes[minor];
			if (atomic_read(&node->needs_sync))
				break;
		}

		if (minor == MAX_DEVS) {
			spin_unlock(&p3op_nodes_lock);
			break;
		} else if (unlikely(atomic_read(&node->dead_heartbeats) >= TIMEOUT_BEATS)) {
			device_destroy(device_class, MKDEV(device_major, minor));
			atomic_long_set(&node->dev, (long)NULL);

			destroyed = 1;
		}

		atomic_set(&node->needs_sync, 0);
		memcpy(&pos, &node->target, sizeof pos);
		memcpy(&peer, &node->addr, sizeof peer);

		spin_unlock(&p3op_nodes_lock);

		if (destroyed) {
			complete(&node->settled);
			pr_info("p3op%d: disconnected\n", minor);

			continue;
		}

		iov.iov_base = &send;
		iov.iov_len = sizeof send;

		send.base = pos.base;
		send.shoulder = pos.shoulder;
		send.elbow = pos.elbow;

		msglen = kernel_sendmsg(p3op_sock, &msg, &iov, 1, sizeof send);
		if (msglen < 0) {
			pr_err("send error: %zd\n", msglen);
			break;
		}
	}
}

static void p3op_data_ready(struct sock *sk)
{
	(void)sk;
	queue_work(p3op_wq, &p3op_rx_work);
}

static int __init p3op_init(void)
{
	int rc;
	struct p3op_node *node;

	pr_info("arm-driver: register_device() is called.\n");

	for (node = p3op_nodes; node < &p3op_nodes[MAX_DEVS]; ++node) {
		timer_setup(&node->timer, p3op_heartbeat, 0);
		mutex_init(&node->open_mutex);
		init_completion(&node->settled);
	}

	rc = register_chrdev(0, "p3op", &p3op_fops);
	if (rc < 0) {
		pr_err("arm-driver:  can't register character device with error code = %i\n", rc);
		goto err_chrdev;
	}

	device_major = rc;
	pr_info("arm-driver: registered character device with major=%d and minors 0..255\n", device_major);

	device_class = class_create(THIS_MODULE, "p3op");
	if (IS_ERR(device_class)) {
		rc = PTR_ERR( device_class );
		pr_err("arm-driver:  can't create device class with errorcode = %d\n", rc);
		goto err_class;
	}

	pr_info("arm-driver: device class created\n");

	INIT_WORK(&p3op_rx_work, p3op_rx);
	INIT_WORK(&p3op_tx_work, p3op_tx);

	p3op_wq = alloc_workqueue("p3op", WQ_UNBOUND, 2);
	if (!p3op_wq) {
		rc = -ENOMEM;
		pr_err("can't create workqueue: %d\n", rc);
		goto err_wq;
	}

	rc = sock_create_kern(&init_net, AF_PACKET, SOCK_DGRAM, htons(ETH_P_P3OP), &p3op_sock);
	if (rc < 0) {
		pr_err("can't create AF_PACKET socket in init_net: %d\n", rc);
		goto err_sock;
	}

	p3op_sock->sk->sk_data_ready = p3op_data_ready;
	return 0;

err_sock:
	destroy_workqueue(p3op_wq);

err_wq:
	class_destroy(device_class);

err_class:
	unregister_chrdev(device_major, "p3op");

err_chrdev:
	return rc;
}

static void __exit p3op_exit(void)
{
	int i;

	pr_info("arm-driver: exiting\n");

	cancel_work_sync(&p3op_rx_work);
	cancel_work_sync(&p3op_tx_work);
	destroy_workqueue(p3op_wq);

	sock_release(p3op_sock);

	spin_lock(&p3op_nodes_lock);
	for (i = 0; i < MAX_DEVS; ++i)
		if ((void *)atomic_long_read(&p3op_nodes[i].dev)) {
			device_destroy(device_class, MKDEV(device_major, i));
			atomic_long_set(&p3op_nodes[i].dev, (long)NULL);
		}
	spin_unlock(&p3op_nodes_lock);

	for (i = 0; i < MAX_DEVS; ++i)
		del_timer_sync(&p3op_nodes[i].timer);

	class_destroy(device_class);
	unregister_chrdev(device_major, "p3op");
}

module_init(p3op_init);
module_exit(p3op_exit);
