use std::net::{Ipv4Addr, UdpSocket};

use clap::{Parser, ValueEnum};
use clap_num::maybe_hex;

#[derive(Parser)]
#[command(version)]
struct Cli {
    ip: Ipv4Addr,
    port: u16,
    #[arg(value_parser=maybe_hex::<u8>)]
    xor_key: u8,
    pin: String,
    #[arg(long, value_enum)]
    layout: Option<Layout>,
}

#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, ValueEnum)]
enum Layout {
    A,
    B,
    C,
}

fn main() -> anyhow::Result<()> {
    let cli = Cli::parse();

    let layout = match cli.layout.unwrap_or(Layout::A) {
        Layout::A => b'a',
        Layout::B => b'b',
        Layout::C => b'c',
    };

    let mut buf = cli.pin.into_bytes();
    buf.push(layout);

    for byte in buf.iter_mut() {
        *byte ^= cli.xor_key;
    }

    UdpSocket::bind((Ipv4Addr::UNSPECIFIED, 0))?.send_to(&buf, (cli.ip, cli.port))?;
    Ok(())
}
