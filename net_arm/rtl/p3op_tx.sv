module p3op_tx
(
	input  logic       clk,
	                   rst_n,

	input  logic       nic_tx_ready,
	output logic       nic_tx,
	                   nic_tx_eop,
	output logic[7:0]  nic_tx_data,

	input  logic[7:0]  av_base,
	                   av_shoulder,
	                   av_elbow
);

	localparam logic[5:0] END_FRAME_INDEX = 21;

	enum int unsigned
	{
		START,
		SEND,
		WAIT
	} state;

	logic[7:0] next_tx;
	logic[5:0] index;
	logic[31:0] wait_count;

	always_comb
		unique case (index)
			6'd0: next_tx = 8'hff;
			6'd1: next_tx = 8'hff;
			6'd2: next_tx = 8'hff;
			6'd3: next_tx = 8'hff;
			6'd4: next_tx = 8'hff;
			6'd5: next_tx = 8'hff;
			6'd6: next_tx = 8'h69;
			6'd7: next_tx = 8'h69;
			6'd8: next_tx = 8'h69;
			6'd9: next_tx = 8'h69;
			6'd10: next_tx = 8'h69;
			6'd11: next_tx = 8'h69;
			6'd12: next_tx = 8'h69; // ethertype
			6'd13: next_tx = 8'h69; // ethertype
			6'd14: next_tx = 8'h70; // 'p'
			6'd15: next_tx = 8'h33; // '3'
			6'd16: next_tx = 8'h6f; // 'o'
			6'd17: next_tx = 8'h70; // 'p'
			6'd18: next_tx = av_base;
			6'd19: next_tx = av_shoulder;
			6'd20: next_tx = av_elbow;
		endcase

	always_ff @(posedge clk or negedge rst_n)
		if (!rst_n) begin
			state <= START;
			index <= 0;
			nic_tx <= 0;
		end else unique case (state)
			START: begin
				state <= SEND;
				index <= 1;
				nic_tx <= 1;
			end

			SEND:
				if (nic_tx_ready) begin
					index <= index + 1;

					if (index == END_FRAME_INDEX) begin
						state <= WAIT;
						nic_tx <= 0;
					end
				end

			WAIT:
				if (wait_count == 0) begin
					state <= START;
					index <= 0;
				end
		endcase

	// No necesitan rst_n
	always_ff @(posedge clk)
		unique case (state)
			START: begin
				nic_tx_eop <= 0;
				nic_tx_data <= next_tx;
			end

			SEND:
				if (nic_tx_ready) begin
					nic_tx_eop <= index == END_FRAME_INDEX - 1;
					nic_tx_data <= next_tx;

					if (index == END_FRAME_INDEX)
						wait_count <= 32'd12500000; // 250ms
				end

			WAIT:
				if (wait_count != 0)
					wait_count <= wait_count - 1;
		endcase

endmodule
