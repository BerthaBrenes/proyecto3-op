module p3op_rx
(
	input  logic       clk,
	                   rst_n,

	input  logic       nic_rx,
	input  logic[7:0]  nic_rx_data,

	output logic[7:0]  sp_base,
	                   sp_shoulder,
	                   sp_elbow
);

	enum int unsigned
	{
		LENGTH,
		HEADER,
		MAGIC,
		DATA,
		DISCARD
	} state;

	logic lo, eop;
	logic[3:0] count;
	logic[7:0] len_hi, magic, hold_base, hold_shoulder;
	logic[15:0] len, next_len;

	assign eop = len <= 3; // Quedan 2 bytes de header + 1 byte actual
	assign next_len = {len[7:0], nic_rx_data};

	always_comb
		unique case (count)
			// Está al revés porque contamos desde 5 a 0
			5'd0: magic = 8'h70; // 'p'
			5'd1: magic = 8'h6f; // 'o'
			5'd2: magic = 8'h33; // '3'
			5'd3: magic = 8'h70; // 'p'
			5'd4: magic = 8'h69; // ethertype
			5'd5: magic = 8'h69; // ethertype
		endcase

	always_ff @(posedge clk or negedge rst_n)
		if (!rst_n) begin
			lo <= 0;
			state <= LENGTH;

			// Todos los servos inician a la mitad de su rango
			sp_base <= 8'h80;
			sp_elbow <= 8'h80;
			sp_shoulder <= 8'h80;
		end else begin
			unique case (state)
				LENGTH:
					if (nic_rx) begin
						lo <= 1;

						if (lo)
							state <= HEADER;
					end

				HEADER:
					if (nic_rx && count == 0)
						state <= MAGIC;

				MAGIC:
					if (nic_rx) begin
						if (count == 0)
							state <= DATA;

						if (nic_rx_data != magic)
							state <= DISCARD;
					end

				DATA:
					if (nic_rx && count == 0) begin
						state <= DISCARD;

						sp_base <= hold_base;
						sp_shoulder <= hold_shoulder;
						sp_elbow <= nic_rx_data;
					end

				DISCARD: ;
			endcase

			if (state != LENGTH && nic_rx && eop) begin
				lo <= 0;
				state <= LENGTH;
			end
		end

	// No necesitan rst_n
	always_ff @(posedge clk) begin
		unique case (state)
			LENGTH:
				if (nic_rx) begin
					len <= next_len;

					if (lo)
						count <= 4'd11;
				end

			HEADER:
				if (nic_rx)
					count <= count != 0 ? count - 1 : 4'd5;

			MAGIC:
				if (nic_rx) 
					count <= count != 0 ? count - 1 : 4'd2;

			DATA:
				if (nic_rx) begin
					hold_base <= hold_shoulder;
					hold_shoulder <= nic_rx_data;

					if (count != 0)
						count <= count - 1;
				end

			DISCARD: ;
		endcase

		if (state != LENGTH && nic_rx)
			len <= len - 1;
	end

endmodule
