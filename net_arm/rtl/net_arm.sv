module net_arm
(
	input  logic      clk_clk,
	                  reset_reset_n,
	                  spi_0_external_MISO,
	output logic      spi_0_external_MOSI,
	                  spi_0_external_SCLK,
	                  spi_0_external_SS_n,
	                  sdcard_SS_n,
				      pwm_base,
	                  pwm_shoulder,
	                  pwm_elbow
);

	logic clk;

	assign clk = clk_clk;
	assign sdcard_SS_n = 1;

	logic[7:0] av_base;

	pwm_driver base
	(
		.av(av_base),
		.sp(sp_base),
		.pwm(pwm_base),
		.*
	);

	logic[7:0] av_shoulder;

	pwm_driver shoulder
	(
		.av(av_shoulder),
		.sp(sp_shoulder),
		.pwm(pwm_shoulder),
		.*
	);

	logic[7:0] av_elbow;

	pwm_driver elbow
	(
		.av(av_elbow),
		.sp(sp_elbow),
		.pwm(pwm_elbow),
		.*
	);

	logic w5100_start, w5100_write, nic_rx, nic_tx_ready;
	logic[7:0] w5100_out, nic_rx_data;
	logic[15:0] w5100_reg;

	nic nic
	(
		.*
	);

	logic nic_tx, nic_tx_eop;
	logic[7:0] nic_tx_data;

	p3op_tx tx
	(
		.*
	);

	logic[7:0] sp_base, sp_shoulder, sp_elbow;

	p3op_rx rx
	(
		.*
	);

	logic rst_n, w5100_ready;
	logic[7:0] w5100_in;

	platform plat
	(
		.rst_0_out_reset_reset_n(rst_n),

		.w5100_master_0_io_w5100_start(w5100_start),
		.w5100_master_0_io_w5100_write(w5100_write),
		.w5100_master_0_io_w5100_ready(w5100_ready),
		.w5100_master_0_io_w5100_reg(w5100_reg),
		.w5100_master_0_io_w5100_in(w5100_in),
		.w5100_master_0_io_w5100_out(w5100_out),

		.*
	);

endmodule
