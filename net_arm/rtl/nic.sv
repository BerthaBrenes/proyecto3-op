module nic
(
	input  logic       clk,
	                   rst_n,

	input  logic[7:0]  w5100_in,
	input  logic       w5100_ready,
	output logic       w5100_start,
	                   w5100_write,
	output logic[7:0]  w5100_out,
	output logic[15:0] w5100_reg,

	input  logic       nic_tx,
	                   nic_tx_eop,
	input  logic[7:0]  nic_tx_data,
	output logic       nic_tx_ready,

	output logic       nic_rx,
	output logic[7:0]  nic_rx_data
);

	localparam logic[4:0] END_PROGRAM_INDEX = 14;

	localparam logic[15:0]
		W5100_MR           = 16'h0000,
		W5100_GWR0         = 16'h0001,
		W5100_GWR1         = 16'h0002,
		W5100_GWR2         = 16'h0003,
		W5100_GWR3         = 16'h0004,
		W5100_SUBR0        = 16'h0005,
		W5100_SUBR1        = 16'h0006,
		W5100_SUBR2        = 16'h0007,
		W5100_SUBR3        = 16'h0008,
		W5100_SHAR0        = 16'h0009,
		W5100_SHAR1        = 16'h000a,
		W5100_SHAR2        = 16'h000b,
		W5100_SHAR3        = 16'h000c,
		W5100_SHAR4        = 16'h000d,
		W5100_SHAR5        = 16'h000e,
		W5100_SIPR0        = 16'h000f,
		W5100_SIPR1        = 16'h0010,
		W5100_SIPR2        = 16'h0011,
		W5100_SIPR3        = 16'h0012,
		W5100_S0_MR        = 16'h0400,
		W5100_S0_CR        = 16'h0401,
		W5100_S0_DHAR0     = 16'h0406,
		W5100_S0_DHAR1     = 16'h0407,
		W5100_S0_DHAR2     = 16'h0408,
		W5100_S0_DHAR3     = 16'h0409,
		W5100_S0_DHAR4     = 16'h040a,
		W5100_S0_DHAR5     = 16'h040b,
		W5100_S0_TX_WR_HI  = 16'h0424,
		W5100_S0_TX_WR_LO  = 16'h0425,
		W5100_S0_RX_RSR_HI = 16'h0426,
		W5100_S0_RX_RSR_LO = 16'h0427,
		W5100_S0_RX_RD_HI  = 16'h0428,
		W5100_S0_RX_RD_LO  = 16'h0429;

	localparam logic[4:0]
		W5100_S0_TX_BUF = {4'h4, 1'b0},
		W5100_S0_RX_BUF = {4'h6, 1'b0};

	localparam logic[7:0]
		W5100_MR_MACRAW = 8'h04,
		W5100_CR_OPEN   = 8'h01,
		W5100_CR_SEND   = 8'h20,
		W5100_CR_RECV   = 8'h40;

	enum int unsigned
	{
		RESET_NIC,
		RESET_W5100,
		PROGRAM,
		IDLE,
		RX_SIZE,
		TX,
		RX,
		TX_EOP,
		RX_EOP,
		TX_PTR,
		RX_PTR,
		END
	} state;

	logic lo;
	logic[4:0] program_index;
	logic[7:0] program_out;
	logic[15:0] program_reg, rx_size, next_rx_size;
	logic[15:0] rx_ptr, tx_ptr;
	logic[47:0] shar, dhar;

	assign shar = {8'd69, 8'd69, 8'd69, 8'd69, 8'd69, 8'd69};
	assign dhar = 48'hffffffffffff;
	assign next_rx_size = {rx_size[7:0], w5100_in};

	always_comb
		unique case (program_index)
			5'd0: begin
				program_reg = W5100_SHAR0;
				program_out = shar[47:40];
			end

			5'd1: begin
				program_reg = W5100_SHAR1;
				program_out = shar[39:32];
			end

			5'd2: begin
				program_reg = W5100_SHAR2;
				program_out = shar[31:24];
			end

			5'd3: begin
				program_reg = W5100_SHAR3;
				program_out = shar[23:16];
			end

			5'd4: begin
				program_reg = W5100_SHAR4;
				program_out = shar[15:8];
			end

			5'd5: begin
				program_reg = W5100_SHAR5;
				program_out = shar[7:0];
			end

			5'd6: begin
				program_reg = W5100_S0_DHAR0;
				program_out = dhar[47:40];
			end

			5'd7: begin
				program_reg = W5100_S0_DHAR1;
				program_out = dhar[39:32];
			end

			5'd8: begin
				program_reg = W5100_S0_DHAR2;
				program_out = dhar[31:24];
			end

			5'd9: begin
				program_reg = W5100_S0_DHAR3;
				program_out = dhar[23:16];
			end

			5'd10: begin
				program_reg = W5100_S0_DHAR4;
				program_out = dhar[15:8];
			end

			5'd11: begin
				program_reg = W5100_S0_DHAR5;
				program_out = dhar[7:0];
			end

			5'd12: begin
				program_reg = W5100_S0_MR;
				program_out = W5100_MR_MACRAW;
			end

			5'd13: begin
				program_reg = W5100_S0_CR;
				program_out = W5100_CR_OPEN;
			end
		endcase

	always_ff @(posedge clk or negedge rst_n)
		if (!rst_n) begin
			state <= RESET_NIC;
			w5100_start <= 0;

			nic_rx <= 0;
			nic_tx_ready <= 0;
		end else unique case (state)
			RESET_NIC: begin
				state <= RESET_W5100;
				w5100_start <= 1;
			end

			RESET_W5100:
				if (w5100_ready)
					state <= PROGRAM;

			PROGRAM:
				if (w5100_ready && program_index == END_PROGRAM_INDEX) begin
					state <= IDLE;
					w5100_start <= 0;
				end

			IDLE: begin
				w5100_start <= 1;

				if (nic_tx) begin
					state <= nic_tx_eop ? TX_EOP : TX;
					nic_tx_ready <= 1;
				end else
					state <= RX_SIZE;
			end

			RX_SIZE:
				if (w5100_ready && lo) begin
					if (next_rx_size != 0)
						state <= RX;
					else begin
						state <= IDLE;
						w5100_start <= 0;
					end
				end

			TX: begin
				nic_tx_ready <= 0;

				if (w5100_ready) begin
					state <= IDLE;
					w5100_start <= 0;
				end
			end

			RX: begin
				nic_rx <= w5100_ready;
				if (w5100_ready && rx_size == 1)
					state <= RX_EOP;
			end

			TX_EOP: begin
				nic_tx_ready <= 0;

				if (w5100_ready && lo)
					state <= TX_PTR;
			end

			RX_EOP: begin
				nic_rx <= 0;
				if (w5100_ready)
					state <= RX_PTR;
			end

			TX_PTR:
				if (w5100_ready)
					state <= END;

			RX_PTR:
				if (w5100_ready)
					state <= END;

			END:
				if (w5100_ready) begin
					state <= IDLE;
					w5100_start <= 0;
				end
		endcase

	// No necesitan rst_n
	always_ff @(posedge clk)
		unique case (state)
			RESET_NIC: begin
				tx_ptr <= 0;
				rx_ptr <= 0;

				w5100_out <= 8'h80; // S/W Reset
				w5100_reg <= W5100_MR;
				w5100_write <= 1;
			end

			RESET_W5100:
				if (w5100_ready) begin
					w5100_out <= 8'h00;
					program_index <= 0;
				end

			// Asume w5100_start && w5100_write
			PROGRAM:
				if (w5100_ready && program_index != END_PROGRAM_INDEX) begin
					w5100_reg <= program_reg;
					w5100_out <= program_out;
					program_index <= program_index + 1;
				end

			IDLE: begin
				lo <= 0;

				if (nic_tx) begin
					w5100_reg <= {W5100_S0_TX_BUF, tx_ptr[10:0]};
					w5100_out <= nic_tx_data;
					w5100_write <= 1;

					tx_ptr <= tx_ptr + 1;
				end else begin
					w5100_reg <= W5100_S0_RX_RSR_HI;
					w5100_write <= 0;
				end
			end

			RX_SIZE:
				if (w5100_ready) begin
					rx_size <= next_rx_size;

					if (lo && next_rx_size != 0) begin
						rx_ptr <= rx_ptr + 1;
						w5100_reg <= {W5100_S0_RX_BUF, rx_ptr[10:0]};
					end else begin
						lo <= 1;
						w5100_reg <= W5100_S0_RX_RSR_LO;
					end
				end

			TX: ;

			RX: begin
				nic_rx_data <= w5100_in;

				if (w5100_ready) begin
					rx_size <= rx_size - 1;

					if (rx_size == 1) begin
						w5100_reg <= W5100_S0_RX_RD_HI;
						w5100_out <= rx_ptr[15:8];
						w5100_write <= 1;
					end else begin
						rx_ptr <= rx_ptr + 1;
						w5100_reg <= {W5100_S0_RX_BUF, rx_ptr[10:0]};
					end
				end
			end

			TX_EOP:
				if (w5100_ready) begin
					lo <= 1;
					w5100_reg <= lo ? W5100_S0_TX_WR_LO : W5100_S0_TX_WR_HI;
					w5100_out <= lo ? tx_ptr[7:0] : tx_ptr[15:8];
				end

			RX_EOP:
				if (w5100_ready) begin
					w5100_reg <= W5100_S0_RX_RD_LO;
					w5100_out <= rx_ptr[7:0];
				end

			TX_PTR:
				if (w5100_ready) begin
					w5100_reg <= W5100_S0_CR;
					w5100_out <= W5100_CR_SEND;
				end

			RX_PTR:
				if (w5100_ready) begin
					w5100_reg <= W5100_S0_CR;
					w5100_out <= W5100_CR_RECV;
				end

			END: ;
		endcase

endmodule
