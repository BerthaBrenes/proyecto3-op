module pwm_driver
(
	input  logic      clk,
	                  rst_n,

	input  logic[7:0] sp,
	output logic      pwm,
	output logic[7:0] av
);

	// 544us y 2400us salen del source code de Servo.h (Arduino)
	localparam logic[15:0]
		BIAS      = 27200, // 544us * 50MHz
		PER_POINT = 362;   // ((2400us - 544us)/180degree)(180degree/256)(50MHz)

	logic pwm_n, run, per_point;
	logic[7:0] point;
	logic[22:0] tick; // Overflow cada 2^23 / 50MHz = 167.772ms
	logic[15:0] count;

	// Por los puentes inversores
	assign pwm = !pwm_n;

	always @(posedge clk or negedge rst_n)
		if (!rst_n) begin
			av <= 0;
			run <= 0;
			tick <= 0;
			point <= 0;
			count <= 0;
			pwm_n <= 0;
			per_point <= 0;
		end else begin
			tick <= tick + 1;

			if (tick == 0) begin
				av <= sp;
				run <= av != sp;
			end

			if (count != 0)
				count <= count - 1;
			else if (point != 0) begin
				point <= point - 1;
				count <= PER_POINT;
			end else begin
				pwm_n <= run && !pwm_n;
				if (run) begin
					count <= BIAS;
					point <= av;
				end
			end
		end

endmodule
