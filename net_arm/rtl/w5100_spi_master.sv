module w5100_spi_master
(
	input  logic       clk,
	                   rst_n,

	input  logic       avm_waitrequest,
	input  logic[31:0] avm_readdata,
	output logic[31:0] avm_writedata,
	output logic[4:0]  avm_address,
	output logic       avm_write,
	                   avm_read,

	input  logic       spi_irq,

	input  logic[15:0] w5100_reg,
	input  logic       w5100_start,
	                   w5100_write,
	input  logic[7:0]  w5100_out,
	output logic[7:0]  w5100_in,
	output logic       w5100_ready
);

	localparam logic[4:0]
		SPI_RXDATA      = 5'h00,
		SPI_TXDATA      = 5'h04,
		SPI_STATUS      = 5'h08,
		SPI_CONTROL     = 5'h0c,
		SPI_SLAVESELECT = 5'h14,
		SPI_EOP_VALUE   = 5'h18;

	enum int unsigned
	{
		RESET,
		UNMASK,
		IDLE,
		TX,
		IRQ,
		RX
	} state;

	assign w5100_in = avm_readdata[7:0];

	always_comb
		unique case (state)
			RX:
				w5100_ready = !avm_waitrequest;

			default:
				w5100_ready = 0;
		endcase

	always_ff @(posedge clk or negedge rst_n)
		if (!rst_n) begin
			state <= RESET;

			avm_read <= 0;
			avm_write <= 0;
		end else unique case (state)
			RESET: begin
				state <= UNMASK;
				avm_write <= 1;
			end

			UNMASK:
				if (!avm_waitrequest) begin
					state <= IDLE;
					avm_write <= 0;
				end

			IDLE:
				if (w5100_start) begin
					state <= TX;
					avm_write <= 1;
				end

			TX:
				if (!avm_waitrequest) begin
					state <= IRQ;
					avm_write <= 0;
				end

			IRQ:
				if (spi_irq) begin
					state <= RX;
					avm_read <= 1;
				end

			RX:
				if (!avm_waitrequest) begin
					state <= IDLE;
					avm_read <= 0;
				end
		endcase

	// No necesitan rst_n
	always_ff @(posedge clk)
		unique case (state)
			RESET: begin
				avm_address <= SPI_CONTROL;
				avm_writedata <= 32'h00000080; // IRRDY
			end

			UNMASK: ;

			IDLE:
				if (w5100_start) begin
					avm_address <= SPI_TXDATA;
					avm_writedata <= {{4{w5100_write}}, {4{!w5100_write}}, w5100_reg, w5100_out};
				end

			TX: ;

			IRQ:
				if (spi_irq)
					avm_address <= SPI_RXDATA;

			RX: ;
		endcase

endmodule
