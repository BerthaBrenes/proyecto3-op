import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CodeService {
  newCode:Array<Number> =[1,2,3,4];

  constructor() { }

  getCode(){
    return this.newCode;
  }
  updateCode(newD: Array<Number>) {
    this.newCode = newD;
  }
}
