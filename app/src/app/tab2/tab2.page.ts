import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { CodeService } from '../code.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  correctNumber: Array<Number> = [3,5,6,9];
  taped:Array<Number> = [];

  constructor(private toastController: ToastController, private codeService: CodeService) {}
  tapNumber(id: number){
    console.log('test', id);
    this.taped.push(id);
    this.presentToast(`${id}`, 'bottom');

  }
  send(){
    console.log('ready', this.taped);
    this.correctNumber = this.codeService.getCode();
    console.log('api', this.correctNumber);
    if(this.taped.toString() === this.correctNumber.toString()){
      this.presentToast('Código correcto', 'middle')
    }else{
      this.presentToast('Código incorrecto', 'middle')
    }
    this.taped = [];

  }
  async presentToast(message: string, position: 'top' | 'middle' | 'bottom') {
    const toast = await this.toastController.create({
      message: message,
      duration: 1500,
      position,
    });

    await toast.present();
  }
}
