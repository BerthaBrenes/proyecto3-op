import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { tabCodePage } from './tabCode.page';
import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { tabCodePageRoutingModule } from './tabCode-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    ExploreContainerComponentModule,
    tabCodePageRoutingModule
  ],
  declarations: [tabCodePage]
})
export class TabCodePageModule {}
