import { ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExploreContainerComponentModule } from '../explore-container/explore-container.module';

import { tabCodePage } from './tabCode.page';

describe('tabCodePage', () => {
  let component: tabCodePage;
  let fixture: ComponentFixture<tabCodePage>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [tabCodePage],
      imports: [IonicModule.forRoot(), ExploreContainerComponentModule]
    }).compileComponents();

    fixture = TestBed.createComponent(tabCodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
