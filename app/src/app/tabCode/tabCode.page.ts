import { Component } from '@angular/core';
import { ToastController } from '@ionic/angular';
import {CodeService } from '../code.service';
@Component({
  selector: 'app-tabCode',
  templateUrl: 'tabCode.page.html',
  styleUrls: ['tabCode.page.scss']
})
export class tabCodePage {
  correctNumber = [3, 5, 6, 9];
  taped: Array<number> = [];
  numero: string = '1234'; // Agregar la variable "numero"

  constructor(private toastController: ToastController, private codeService: CodeService) {}

  tapNumber(id: number) {
    console.log('test', id);
    this.taped.push(id);
  }

  send() {
    console.log('ready', this.taped);
    
    this.numero = this.taped.toString();
    console.log('Número ingresado:', this.numero); // Acceder al número ingresado

    this.codeService.updateCode(this.taped);
    this.presentToast(`Nuevo codigo: ${this.taped}`);
    this.taped = [];
    
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 1500,
      position: 'middle'
    });

    await toast.present();
  }
}
