import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { tabCodePage } from './tabCode.page';

const routes: Routes = [
  {
    path: '',
    component: tabCodePage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class tabCodePageRoutingModule {}
