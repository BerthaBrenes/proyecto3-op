#define _GNU_SOURCE

#include <arpa/inet.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <unistd.h>

#include <libp3op.h>

#define BUF_MAX 16

static sig_atomic_t ctrl_c = 0;

void handle_sigint(int sig)
{
	(void)sig;
	ctrl_c = 1;
}

static void print_sockaddr(const struct sockaddr_in *addr)
{
	char buf[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &addr->sin_addr, buf, sizeof buf);
	fputs(buf, stderr);
	fprintf(stderr, ":%d", ntohs(addr->sin_port));
}

static void p3op_perror(const struct p3op *p3op, const char *fmt, ...)
{
	va_list args;
	va_start(args, fmt);

	vfprintf(stderr, fmt, args);
	fprintf(stderr, ": %s\n", p3op_errmsg(p3op));

	va_end(args);
}

static int worker_main(uint8_t dev_no, uint8_t xor_key, char pin[BUF_MAX])
{
	int set_layout = 0;
	size_t size = 0, index = 0;
	enum p3op_key keys[BUF_MAX - 1];
	enum p3op_layout layout;

	while (pin[index]) {
		enum p3op_key key;
		switch ((pin[index++] ^= xor_key)) {
			case '0': key = P3OP_KEY_K0; break;
			case '1': key = P3OP_KEY_K1; break;
			case '2': key = P3OP_KEY_K2; break;
			case '3': key = P3OP_KEY_K3; break;
			case '4': key = P3OP_KEY_K4; break;
			case '5': key = P3OP_KEY_K5; break;
			case '6': key = P3OP_KEY_K6; break;
			case '7': key = P3OP_KEY_K7; break;
			case '8': key = P3OP_KEY_K8; break;
			case '9': key = P3OP_KEY_K9; break;
			case '*': key = P3OP_KEY_STAR; break;
			case '#': key = P3OP_KEY_POUND; break;

			case 'a':
				layout = P3OP_LAYOUT_A;
				set_layout = 1;
				continue;

			case 'b':
				layout = P3OP_LAYOUT_B;
				set_layout = 1;
				continue;

			case 'c':
				layout = P3OP_LAYOUT_C;
				set_layout = 1;
				continue;

			default:
				fprintf(stderr, "Failed to decipher pin (bad key?)\n");
				return EXIT_FAILURE;
		}

		keys[size++] = key;
	}

	fprintf(stderr, "Pin is '%s'\n", pin);

	int ret = EXIT_SUCCESS;

	struct p3op p3op;
	p3op_new(&p3op);

	if (p3op_dev_open(&p3op, dev_no) < 0) {
		p3op_perror(&p3op, "p3op_open(%hhu)", dev_no);
		ret = EXIT_FAILURE;
		goto exit_p3op;
	} else if (set_layout && p3op_dev_switch_layout(&p3op, layout) < 0) {
		p3op_perror(&p3op, "p3op_dev_set_layout()");
		ret = EXIT_FAILURE;
		goto exit_dev;
	} else if (p3op_dev_type_keys(&p3op, keys, size) < 0) {
		p3op_perror(&p3op, "p3op_dev_type_keys()");
		ret = EXIT_FAILURE;
		goto exit_dev;
	} else if (p3op_dev_confirm(&p3op) < 0) {
		p3op_perror(&p3op, "p3op_confirm()");
		ret = EXIT_FAILURE;
		goto exit_dev;
	}

exit_dev:
	if (p3op_dev_close(&p3op) < 0) {
		p3op_perror(&p3op, "p3op_close(%hhu)", dev_no);
		ret = EXIT_FAILURE;
		goto exit_p3op;
	}

exit_p3op:
	p3op_drop(&p3op);
	return ret;
}

static int probe_dev(uint8_t dev_no)
{
	struct p3op p3op;
	p3op_new(&p3op);

	int ret = 1;
	if (p3op_dev_open(&p3op, dev_no) < 0) {
		p3op_perror(&p3op, "probe_dev(): p3op_open(%hhu)", dev_no);
		ret = 0;
	} else if (p3op_dev_close(&p3op) < 0) {
		p3op_perror(&p3op, "probe_dev(): p3op_close(%hhu)", dev_no);
		ret = 0;
	}

	p3op_drop(&p3op);
	return ret;
}

int main(int argc, const char *argv[])
{
	struct sigaction sigint, sigchld;

	memset(&sigint, 0, sizeof sigint);
	memset(&sigchld, 0, sizeof sigchld);

	sigemptyset(&sigint.sa_mask);
	sigemptyset(&sigchld.sa_mask);

	sigint.sa_handler = handle_sigint;
	sigchld.sa_flags = SA_NOCLDSTOP | SA_NOCLDWAIT;

	if (sigaction(SIGINT, &sigint, NULL) < 0 || sigaction(SIGCHLD, &sigchld, NULL) < 0) {
		perror("sigaction()");
		return EXIT_FAILURE;
	}

	struct sockaddr_in addr;
	memset(&addr, 0, sizeof addr);
	addr.sin_family = AF_INET;

	char *endptr;
	unsigned dev_no, xor_key;

	if (argc != 5 || !inet_aton(argv[1], &addr.sin_addr)
	 || ((addr.sin_port = htons(strtoul(argv[2], &endptr, 0))), *endptr)
	 || ((dev_no = strtoul(argv[3], &endptr, 0)), *endptr)
	 || dev_no > UINT8_MAX
	 || ((xor_key = strtoul(argv[4], &endptr, 0)), *endptr)
	 || xor_key > UINT8_MAX)
	{
		fprintf(stderr, "usage: %s <addr> <port> <device no> <xor key>\n", argv[0]);
		return EXIT_FAILURE;
	}

	int ret = EXIT_SUCCESS;

	int sock;
	if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
		perror("socket()");
		return EXIT_FAILURE;
	} else if (bind(sock, (const struct sockaddr *)&addr, sizeof addr) < 0) {
		perror("bind()");
		ret = EXIT_FAILURE;
		goto exit_socket;
	} else if (!probe_dev((uint8_t)dev_no)) {
		fprintf(stderr, "Failed to probe device %u\n", dev_no);
		ret = EXIT_FAILURE;
		goto exit_socket;
	}

	fputs("Listening on UDP ", stderr);
	print_sockaddr(&addr);
	fputs("...\n", stderr);

	while (1) {
		char buf[BUF_MAX];
		struct sockaddr_in peer;
		socklen_t len = sizeof peer;

		ssize_t size;
		if ((size = recvfrom(sock, buf, sizeof buf - 1, MSG_TRUNC,
		                     (struct sockaddr *)&peer, &len)) < 0)
		{
			if (errno == EINTR) {
				if (ctrl_c)
					break;

				continue;
			}

			perror("recvfrom()");
			ret = EXIT_FAILURE;
			goto exit_children;
		}

		fprintf(stderr, "Received %zd-bytes datagram from ", size);
		print_sockaddr(&peer);
		fputc('\n', stderr);

		if (!size) {
			fputs("Dropping null message\n", stderr);
			continue;
		} else if ((size_t)size >= BUF_MAX) {
			fputs("Dropping truncated message\n", stderr);
			continue;
		}

		buf[size] = '\0';

		pid_t pid;
		if ((pid = fork()) < 0) {
			perror("fork()");
			ret = EXIT_FAILURE;
			goto exit_children;
		} else if (!pid)
			exit(worker_main((uint8_t)dev_no, (uint8_t)xor_key, buf));
	}

exit_children:
	fputs("Waiting for all children to exit...\n", stderr);

	while (wait(NULL) >= 0)
		continue;

	if (errno != ECHILD) {
		perror("wait()");
		ret = EXIT_FAILURE;
	}

exit_socket:
	close(sock);
	return ret;
}
