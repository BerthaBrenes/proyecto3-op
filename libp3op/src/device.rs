use std::{cmp::Ordering, fmt};

use super::{
    io::{Node, Pos},
    Key, Layout, P3OpError,
};

pub struct Device {
    pos: Pos,
    node: Node,
    layout: Layout,
}

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
pub struct DeviceNo(pub u8);

impl Device {
    pub fn open(no: DeviceNo) -> Result<Self, P3OpError> {
        let mut node = Node::open(no)?;
        let pos = node.read()?;

        Ok(Self {
            pos,
            node,
            layout: Layout::default(),
        })
    }

    pub fn close(&mut self) -> Result<(), P3OpError> {
        self.node.close()
    }

    pub fn switch_layout(&mut self, layout: Layout) {
        self.layout = layout;
    }

    pub fn type_keys(&mut self, keys: &[Key]) -> Result<(), P3OpError> {
        for key in keys.iter() {
            self.type_pos(self.layout.map(*key))?;
        }

        Ok(())
    }

    pub fn confirm(&mut self) -> Result<(), P3OpError> {
        self.type_pos(self.layout.enter())
    }

    fn type_pos(&mut self, target: Pos) -> Result<(), P3OpError> {
        const STEP: u8 = 10;
        const PEN_RAISE: u8 = 15;
        const PEN_TOUCH: u8 = 10;

        let up_pen = Pos {
            shoulder: self.pos.shoulder.saturating_sub(PEN_RAISE),
            ..self.pos
        };

        self.node.write(up_pen)?;
        self.pos = up_pen;

        let move_base = Pos {
            base: target.base,
            ..self.pos
        };

        self.node.write(move_base)?;
        self.pos = move_base;

        while self.pos.shoulder != target.shoulder || self.pos.elbow != target.elbow {
            let elbow = match target.elbow.cmp(&self.pos.elbow) {
                Ordering::Equal => self.pos.elbow,
                Ordering::Less => self.pos.elbow.saturating_sub(STEP).max(target.elbow),
                Ordering::Greater => self.pos.elbow.saturating_add(STEP).min(target.elbow),
            };

            let next = Pos { elbow, ..self.pos };
            self.node.write(next)?;
            self.pos = next;

            let shoulder = match target.shoulder.cmp(&self.pos.shoulder) {
                Ordering::Equal => self.pos.shoulder,
                Ordering::Less => self.pos.shoulder.saturating_sub(STEP).max(target.shoulder),
                Ordering::Greater => self.pos.shoulder.saturating_add(STEP).min(target.shoulder),
            };

            let next = Pos {
                shoulder,
                ..self.pos
            };
            self.node.write(next)?;
            self.pos = next;
        }

        let hover = self.pos;
        let up_pen = Pos {
            shoulder: self.pos.shoulder.saturating_sub(PEN_TOUCH),
            ..self.pos
        };

        self.node.write(up_pen)?;
        self.pos = up_pen;

        self.node.write(hover)?;
        self.pos = hover;

        Ok(())
    }
}

impl fmt::Display for DeviceNo {
    fn fmt(&self, fmt: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(fmt, "/dev/p3op{}", self.0)
    }
}
