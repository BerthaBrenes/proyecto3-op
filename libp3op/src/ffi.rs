#![allow(non_camel_case_types)]

use std::{
    ffi::{c_char, c_int, CString},
    mem::MaybeUninit,
    ptr::NonNull,
};

use super::*;

#[repr(C)]
pub struct p3op {
    dev: Option<Box<p3op_device>>,
    error_msg: Option<NonNull<c_char>>,
    error_no: c_int,
}

pub struct p3op_device(Device);

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
#[repr(C)]
pub enum p3op_layout {
    A,
    B,
    C,
}

#[derive(Copy, Clone, Debug, Eq, Hash, PartialEq)]
#[repr(C)]
pub enum p3op_key {
    K0,
    K1,
    K2,
    K3,
    K4,
    K5,
    K6,
    K7,
    K8,
    K9,
    Star,
    Pound,
}

impl p3op {
    fn dev(&mut self) -> Result<&mut Device, P3OpError> {
        self.dev
            .as_mut()
            .map(|dev| &mut dev.0)
            .ok_or(P3OpError::NoDevice)
    }

    fn set_err<F>(&mut self, f: F) -> c_int
    where
        F: FnOnce(&mut Self) -> Result<(), P3OpError>,
    {
        match f(self) {
            Ok(()) => 0,

            Err(err) => {
                let error_msg = CString::new(err.to_string().as_str()).expect("NUL byte in errmsg");

                self.error_no = err.errno();
                self.error_msg = NonNull::new(error_msg.into_raw());

                -1
            }
        }
    }
}

impl Drop for p3op {
    fn drop(&mut self) {
        if let Some(error_msg) = self.error_msg.take() {
            unsafe {
                let _ = CString::from_raw(error_msg.as_ptr());
            }
        }
    }
}

#[no_mangle]
pub unsafe extern "C" fn p3op_new(p3op: *mut MaybeUninit<p3op>) {
    (*p3op).write(p3op {
        dev: None,
        error_msg: None,
        error_no: 0,
    });
}

#[no_mangle]
pub unsafe extern "C" fn p3op_drop(p3op: *mut p3op) {
    p3op.drop_in_place()
}

#[no_mangle]
pub unsafe extern "C" fn p3op_errno(p3op: *const p3op) -> c_int {
    (*p3op).error_no
}

#[no_mangle]
pub unsafe extern "C" fn p3op_errmsg(p3op: *const p3op) -> *const c_char {
    static EMPTY: &[c_char] = &[0];

    match (*p3op).error_msg {
        None => EMPTY.as_ptr(),
        Some(error_msg) => error_msg.as_ptr() as _,
    }
}

#[no_mangle]
pub unsafe extern "C" fn p3op_dev_open(p3op: *mut p3op, dev_no: u8) -> c_int {
    let dev_no = DeviceNo(dev_no);

    (*p3op).set_err(|p3op| {
        let dev = Device::open(dev_no)?;
        p3op.dev = Some(Box::new(p3op_device(dev)));

        Ok(())
    })
}

#[no_mangle]
pub unsafe extern "C" fn p3op_dev_close(p3op: *mut p3op) -> c_int {
    (*p3op).set_err(|p3op| {
        p3op.dev()?.close()?;
        Ok(())
    })
}

#[no_mangle]
pub unsafe extern "C" fn p3op_dev_switch_layout(p3op: *mut p3op, layout: p3op_layout) -> c_int {
    (*p3op).set_err(|p3op| {
        p3op.dev()?.switch_layout(layout);
        Ok(())
    })
}

#[no_mangle]
pub unsafe extern "C" fn p3op_dev_type_keys(
    p3op: *mut p3op,
    keys: *const p3op_key,
    len: usize,
) -> c_int {
    let keys = core::slice::from_raw_parts(keys, len);
    (*p3op).set_err(|p3op| p3op.dev()?.type_keys(keys))
}

#[no_mangle]
pub unsafe extern "C" fn p3op_dev_confirm(p3op: *mut p3op) -> c_int {
    (*p3op).set_err(|p3op| p3op.dev()?.confirm())
}
