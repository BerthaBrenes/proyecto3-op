use std::ffi::c_int;

use nix::errno::Errno;
use thiserror::Error;

use super::DeviceNo;

#[derive(Debug, Error)]
pub enum P3OpError {
    #[error("cannot open device node {no}: {err}")]
    Open { no: DeviceNo, err: Errno },

    #[error("I/O error: {0:?}")]
    Io(Errno),

    #[error("no open device")]
    NoDevice,
}

use P3OpError::*;

impl P3OpError {
    pub(crate) fn errno(&self) -> c_int {
        match self {
            Open { err, .. } => *err as _,
            Io(err) => *err as _,
            NoDevice => Errno::ENODEV as _,
        }
    }
}
