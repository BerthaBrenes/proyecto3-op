use std::{os::fd::RawFd, path::PathBuf};

use nix::{
    fcntl::{open, OFlag},
    sys::stat::Mode,
    unistd::{close, read, write},
};

use super::{DeviceNo, P3OpError};

pub struct Node {
    fd: RawFd,
}

#[derive(Copy, Clone, Debug)]
pub struct Pos {
    pub base: u8,
    pub shoulder: u8,
    pub elbow: u8,
}

impl Node {
    pub fn open(no: DeviceNo) -> Result<Self, P3OpError> {
        let path = PathBuf::from(no.to_string());

        let fd =
            open(&path, OFlag::O_RDWR, Mode::empty()).map_err(|err| P3OpError::Open { err, no })?;

        Ok(Self { fd })
    }

    pub fn close(&mut self) -> Result<(), P3OpError> {
        close(core::mem::replace(&mut self.fd, -1)).map_err(P3OpError::Io)
    }

    pub fn read(&mut self) -> Result<Pos, P3OpError> {
        let mut buf = [0; 3];
        let _ = read(self.fd, &mut buf).map_err(P3OpError::Io)?;

        Ok(Pos {
            base: buf[0],
            shoulder: buf[1],
            elbow: buf[2],
        })
    }

    pub fn write(&mut self, pos: Pos) -> Result<(), P3OpError> {
        let buf = [pos.base, pos.shoulder, pos.elbow];
        let _ = write(self.fd, &buf).map_err(P3OpError::Io)?;

        Ok(())
    }
}

impl Drop for Node {
    fn drop(&mut self) {
        let _ = self.close();
    }
}
