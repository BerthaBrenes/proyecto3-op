pub use super::ffi::{p3op_key as Key, p3op_layout as Layout};

use super::io::Pos;
use {Key::*, Layout::*};

impl Layout {
    pub(crate) fn map(self, key: Key) -> Pos {
        match (self, key) {
            (A, K0) => Pos {
                base: 104,
                shoulder: 205,
                elbow: 103,
            },

            (A, K1) => Pos {
                base: 135,
                shoulder: 245,
                elbow: 180,
            },

            (A, K2) => Pos {
                base: 125,
                shoulder: 240,
                elbow: 175,
            },

            (A, K3) => Pos {
                base: 110,
                shoulder: 245,
                elbow: 180,
            },

            (A, K4) => Pos {
                base: 137,
                shoulder: 227,
                elbow: 152,
            },

            (A, K5) => Pos {
                base: 125,
                shoulder: 225,
                elbow: 150,
            },

            (A, K6) => Pos {
                base: 110,
                shoulder: 225,
                elbow: 155,
            },

            (A, K7) => Pos {
                base: 140,
                shoulder: 215,
                elbow: 135,
            },

            (A, K8) => Pos {
                base: 125,
                shoulder: 215,
                elbow: 135,
            },

            (A, K9) => Pos {
                base: 110,
                shoulder: 215,
                elbow: 135,
            },

            (A, Star) => Pos {
                base: 125,
                shoulder: 204,
                elbow: 106,
            },

            (A, Pound) => Pos {
                base: 143,
                shoulder: 200,
                elbow: 91,
            },

            (B, K0) => Pos {
                base: 124,
                shoulder: 219,
                elbow: 132,
            },

            (B, K1) => Pos {
                base: 136,
                shoulder: 243,
                elbow: 169,
            },

            (B, K2) => Pos {
                base: 122,
                shoulder: 242,
                elbow: 166,
            },

            (B, K3) => Pos {
                base: 112,
                shoulder: 240,
                elbow: 167,
            },

            (B, K4) => Pos {
                base: 138,
                shoulder: 234,
                elbow: 156,
            },

            (B, K5) => Pos {
                base: 120,
                shoulder: 235,
                elbow: 156,
            },

            (B, K6) => Pos {
                base: 107,
                shoulder: 234,
                elbow: 156,
            },

            (B, K7) => Pos {
                base: 137,
                shoulder: 233,
                elbow: 143,
            },

            (B, K8) => Pos {
                base: 122,
                shoulder: 230,
                elbow: 143,
            },

            (B, K9) => Pos {
                base: 109,
                shoulder: 226,
                elbow: 147,
            },

            (B, Star) => Pos {
                base: 132,
                shoulder: 222,
                elbow: 135,
            },

            (B, Pound) => Pos {
                base: 112,
                shoulder: 222,
                elbow: 135,
            },

            (C, K0) => Pos {
                base: 130,
                shoulder: 218,
                elbow: 129,
            },

            (C, K1) => Pos {
                base: 139,
                shoulder: 247,
                elbow: 174,
            },

            (C, K2) => Pos {
                base: 128,
                shoulder: 244,
                elbow: 170,
            },

            (C, K3) => Pos {
                base: 115,
                shoulder: 244,
                elbow: 172,
            },

            (C, K4) => Pos {
                base: 139,
                shoulder: 233,
                elbow: 156,
            },

            (C, K5) => Pos {
                base: 128,
                shoulder: 236,
                elbow: 153,
            },

            (C, K6) => Pos {
                base: 119,
                shoulder: 232,
                elbow: 157,
            },

            (C, K7) => Pos {
                base: 142,
                shoulder: 226,
                elbow: 146,
            },

            (C, K8) => Pos {
                base: 128,
                shoulder: 222,
                elbow: 142,
            },

            (C, K9) => Pos {
                base: 116,
                shoulder: 225,
                elbow: 142,
            },

            (C, Star) => Pos {
                base: 136,
                shoulder: 222,
                elbow: 133,
            },

            (C, Pound) => Pos {
                base: 118,
                shoulder: 219,
                elbow: 135,
            },
        }
    }

    pub fn enter(self) -> Pos {
        match self {
            A => Pos {
                base: 128,
                shoulder: 199,
                elbow: 85,
            },

            B => Pos {
                base: 128,
                shoulder: 216,
                elbow: 118,
            },

            C => Pos {
                base: 128,
                shoulder: 218,
                elbow: 114,
            },
        }
    }
}

impl Default for Layout {
    fn default() -> Self {
        Self::A
    }
}
