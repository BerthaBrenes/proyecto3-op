use std::{env, path::PathBuf};

use cbindgen::{Config, EnumConfig, ExportConfig, Language, MangleConfig, RenameRule, Style};

fn main() {
    let crate_dir = env::var("CARGO_MANIFEST_DIR").unwrap();

    let package_name = env::var("CARGO_PKG_NAME").unwrap();
    let output_file = target_dir()
        .join(format!("lib{}.h", package_name))
        .display()
        .to_string();

    let config = Config {
        language: Language::C,
        include_guard: Some("LIBP3OP_H".into()),
        include_version: true,
        cpp_compat: true,
        usize_is_size_t: true,
        style: Style::Tag,
        enumeration: EnumConfig {
            rename_variants: RenameRule::QualifiedScreamingSnakeCase,
            ..Default::default()
        },
        export: ExportConfig {
            mangle: MangleConfig {
                rename_types: RenameRule::SnakeCase,
                ..Default::default()
            },
            ..Default::default()
        },
        ..Default::default()
    };

    cbindgen::generate_with_config(crate_dir, config)
        .unwrap()
        .write_to_file(output_file);
}

fn target_dir() -> PathBuf {
    if let Ok(target) = env::var("CARGO_TARGET_DIR") {
        PathBuf::from(target)
    } else {
        PathBuf::from(env::var("CARGO_MANIFEST_DIR").unwrap()).join("target")
    }
}
