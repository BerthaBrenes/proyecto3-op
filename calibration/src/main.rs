use clap::Parser;

use p3op::{Device, DeviceNo};

#[derive(Parser)]
#[command(version)]
struct Cli {
    x: f32,
    y: f32,
    z: f32,
}

fn main() -> anyhow::Result<()> {
    let cli = Cli::parse();

    let mut dev = Device::open(DeviceNo(0))?;
    dev.go_to(cli.x, cli.y, cli.z)?;
    dev.close()?;

    Ok(())
}
